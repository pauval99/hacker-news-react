FROM node:14.14

ENV PATH /app/node_modules/.bin:$PATH

COPY ./app /app
COPY ./entrypoint.sh /entrypoint.sh

WORKDIR /app

RUN npm install

ENTRYPOINT [ "/entrypoint.sh" ]
CMD ["npm", "start"]