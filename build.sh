#!/bin/bash

docker build -f DockerfileProd -t pauval99/hacker-news_react:$1 .
docker build -f DockerfileProd -t pauval99/hacker-news_react:latest .

docker push pauval99/hacker-news_react:$1
docker push pauval99/hacker-news_react:latest

docker rmi pauval99/hacker-news_react:$1
docker rmi pauval99/hacker-news_react:latest