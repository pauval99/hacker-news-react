import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import "./App.css";
import APIService from "./services/APIService";

import Submit from "./components/Submit";
import IndexContributions from "./controllers/IndexContributions";
import NewContributions from "./controllers/NewContributions";
import AskContributions from "./controllers/AskContributions";
import UserComments from "./controllers/UserComments";
import User from "./controllers/User";
import UpvotedContributions from "./controllers/UpvotedContributions";
import UpvotedComments from "./controllers/UpvotedComments";
import UserContributions from "./controllers/UserContributions";
import Reply from "./components/comments/Reply";
import CommentForm from "./components/contributions/CommentForm";
import EditContribution from "./components/contributions/EditContribution";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      user: {}
    };
  }

  componentDidMount() {
    APIService.get('/users/1').then(
      response => {
        this.setState({
          user: response.data,
          loading: false,
        });
      }
    );
  }

  render() {
    const { loading, user } = this.state;
    return (
      loading ? 
        <>
        </>
      :
        <>
          <div style={{ height: '10px' }}></div>
          <div className="col-md">
            <div className="container">
              <nav className="mb-1 nav navbar-expand-lg navbar-light bg-hacker-news">
                <ul className="navbar-nav ml-2">
                  <li className="nav-item"><a className="navbar-brand" style={{fontWeight: 'bold'}} href="/contributions">Hacker News</a></li>
                  <li className="nav-item"><a className="nav-link" href="/contributions/new">News</a></li>
                  <li className="nav-item"><a className="nav-link" href="/comments/threads">Threads</a></li>
                  <li className="nav-item"><a className="nav-link" href="/contributions/ask">Ask</a></li>
                  <li className="nav-item"><a className="nav-link" href="/contributions/submit">Submit</a></li>
                </ul>
                <ul className="navbar-nav ml-auto nav-flex-icons">
                  <li className="nav-item">
                    <a className="nav-link" href="/users/1">
                      { user.username } ({ user.karma })
                    </a>
                  </li>
                </ul>
              </nav>
              <Switch>
                <Route exact path="/">
                  <Redirect to="/contributions" />
                </Route>
                <Route exact path="/contributions" component={IndexContributions} />
                <Route exact path="/contributions/submit" component={Submit} />
                <Route exact path="/contributions/new" component={NewContributions} />
                <Route exact path="/contributions/ask" component={AskContributions} />
                <Route exact path="/contribution/:id/edit" component={EditContribution} />

                <Route exact path="/comments/threads" component={UserComments} />
                <Route exact path="/comment/:id" component={CommentForm} />
                <Route exact path="/reply/:id" component={Reply} />

                <Route exact path="/users/:id" component={User} />
                <Route exact path="/users/:id/contributions" component={UserContributions} />
                <Route exact path="/users/:id/comments" component={UserComments} />
                <Route exact path="/user/contributions/voted" component={UpvotedContributions} />
                <Route exact path="/user/comments/voted" component={UpvotedComments} />
                <Route component={() => <h3 className="mt-4" align="center">404 Not Found</h3>} />
              </Switch>
            </div>
          </div>
        </>
    );
  }
}

export default App;
