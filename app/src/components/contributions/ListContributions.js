import React, { Component } from "react";
import Contribution from "./Contribution";

export default class ListContributions extends Component {
  constructor(props) {
    super(props);

    this.state = {
      contributions: props.contributions
    };
  }

  render() {    
    let index = 0;
    const { contributions } = this.state;

    return (
      <div className="row ml-1">
        <div className="col">
          { contributions.map((contribution) => {
              index++;
              return <Contribution key={contribution.id} index={index} contribution={contribution} />
            })
          }
        </div>
      </div>
    );
  }
}