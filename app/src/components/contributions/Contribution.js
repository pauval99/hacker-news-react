import React, { Component } from "react";
import Moment from "moment";
import APIService from "../../services/APIService";

export default class Contribution extends Component {
  constructor(props) {
    super(props);
    this.onClickVote = this.onClickVote.bind(this);
    this.onClickUnvote = this.onClickUnvote.bind(this);
    this.onClickDelete = this.onClickDelete.bind(this);

    this.state = {
      contribution: props.contribution,
      index: props.index === undefined ? false : props.index,
      deleteButton: props.deleteButton === undefined ? true : props.deleteButton,
    }
  }

  componentDidUpdate(prevProps) {
    if(this.props.contribution !== prevProps.contribution) {
      this.setState({
        contribution: this.props.contribution
      }); 
    }
  } 

  onClickDelete() {
    APIService.delete('/contributions/' + this.state.contribution.id).then(
      response => {
        window.location.reload();
      }
    );
  }

  onClickUnvote() {
    APIService.delete('/contributions/' + this.state.contribution.id + '/votes').then(
      response => {
        this.setState({
          contribution: response.data,
        });
      }
    );
  }

  onClickVote() {
    APIService.post('/contributions/' + this.state.contribution.id + '/votes').then(
      response => {
        this.setState({
          contribution: response.data,
        });
      }
    );
  }

  renderStatus(status) {
    const htmlStatus = {
      voted: <span className="titleUser mr-1">&nbsp;&nbsp;&nbsp;&nbsp;</span>,
      unvoted: <div className="title clickable mr-1" onClick={ this.onClickVote }>▲</div>,
      owner: <span className="titleUser">&nbsp;*&nbsp;&nbsp;</span>
    };
    return htmlStatus[status];
  }

  render() {    
    const { index, contribution, deleteButton } = this.state;
    return  (
      <table>
        <tbody>
          <tr>
            { index ?
                <td className="title">{ index }.</td>
              :
                <td></td>
            }
            <td>
              { this.renderStatus(contribution.status) }
            </td>
            <td className="title">
              <a 
              className="link"
              href={ 
                contribution.type === 'url' ? 
                  contribution.url
                : 
                  '/comment/' + contribution.id
              }>
                { contribution.title }
              </a>
              &nbsp;
              { contribution.type === 'url' &&
                <span className="yclinks">
                  ({new URL(contribution.url).hostname})
                </span>
              }
            </td>
          </tr>
          <tr>
            <td colSpan="2"></td>
            <td className="subtext">
              <span className="subtext">
                { contribution.score + ' points by ' }
              </span>
              <a className="subtext" href={ '/users/' + contribution.user.id } >
                { contribution.user.username }
              </a>
              &nbsp;
              <a className="subtext" href={ '/comment/' + contribution.id }>
                { Moment(contribution.created_at).fromNow() }
              </a>
              &nbsp;
              { contribution.status === 'voted' &&
                <>
                  <span className = "subtext">|</span>
                  &nbsp;
                  <span className="subtext clickable" onClick={ this.onClickUnvote }>unvote</span>
                  &nbsp;
                </>
              }
              { contribution.status === 'owner' &&
                <>
                  <span className = "subtext">|</span>
                  &nbsp;
                  <a className="subtext" href={ '/contribution/' + contribution.id + '/edit' }>edit</a>
                  &nbsp;
                  { deleteButton &&
                    <>
                      <span className="subtext">|</span>
                      &nbsp;
                      <span className="subtext clickable" onClick={ this.onClickDelete }>delete</span>
                      &nbsp;
                    </>
                  }
                </>
              }
              <span className = "subtext">|</span>&nbsp;
              <a className="subtext" href={ '/comment/' + contribution.id }>
                { contribution.commentsCount + ' comments' }
              </a>
            </td>
          </tr>
          <tr id="pagespace" style={{height: '10px'}}></tr>
        </tbody>
      </table>
    );
  }
}