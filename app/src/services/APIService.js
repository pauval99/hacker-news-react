import axios from "axios";

const API_URL = "http://54.204.83.10.xip.io/api";
const token = 'Bearer gtcnk6KsgfqFNbsjDYdrnLJBQI7RrsaNL8JbjguO7vYu1FSCkGbJiTBRUied';
const headers = {
    Accept: 'application/json',
    Authorization: token 
}

class APIService {
    get(route) {
        return axios.get(API_URL + route, { headers: headers });
    }
    
    post(route, body) {
        return axios.post(API_URL + route, body, { headers: headers });
    }

    put(route, body) {
        return axios.put(API_URL + route, body, { headers: headers });
    }

    delete(route) {
        return axios.delete(API_URL + route, { headers: headers });
    }
}

export default new APIService();
