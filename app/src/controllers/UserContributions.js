import React, { Component } from "react";
import APIService from "../services/APIService";
import CircularProgress from '@material-ui/core/CircularProgress';

import ListContributions from "../components/contributions/ListContributions";

class UserContributions extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      loading: true,
      contributions: []
    };
  }

  componentDidMount() {
    APIService.get('/users/' + this.props.match.params.id + '/contributions').then(
      response => {
        this.setState({
          contributions: response.data.sort((a, b) => new Date(b.created_at).getTime() - new Date(a.created_at).getTime()),
          loading: false
        });
      }
    );
  }

  render() {
    const { loading, contributions } = this.state;
    return (
      loading ? 
        <div style={{display: 'flex', justifyContent: 'center', marginTop: '200px' }}>
          <CircularProgress />
        </div>
      :
        <ListContributions contributions={ contributions } />
    );
  }
}

export default UserContributions;
