import React, { Component } from "react";
import APIService from "../services/APIService";
import CircularProgress from '@material-ui/core/CircularProgress';

import ListContributions from "../components/contributions/ListContributions";

class IndexContributions extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      loading: true,
      contributions: []
    };
  }

  componentDidMount() {
    APIService.get('/contributions/types/url').then(
      response => {
        this.setState({
          contributions: response.data.sort((a, b) => b.score -  a.score),
          loading: false
        });
      }
    );
  }

  render() {
    const { loading, contributions } = this.state;
    return (
      loading ? 
        <div style={{display: 'flex', justifyContent: 'center', marginTop: '200px' }}>
          <CircularProgress />
        </div>
      :
        <ListContributions contributions={ contributions } />
    );
  }
}

export default IndexContributions;
