## Team
 + Pol Aguilar
 + Sheila Sanchez
 + Sebastian Acurio
 + Pau Val

**URL:** [http://3.85.190.206](http://3.85.190.206) Down

**API:** [http://54.204.83.10.xip.io/api](http://54.204.83.10.xip.io/api) Down

**Swagger:** [https://app.swaggerhub.com/apis-docs/hacker-news/hacker-news/3.0.1](https://app.swaggerhub.com/apis-docs/hacker-news/hacker-news/3.0.1)

## Develop

### With Docker

```sh
sudo apt install docker docker-compose #install docker
docker-compose up --build #web available in localhost:8080
```

### With Npm

```sh
cd app/
npm install -f
npm start
```